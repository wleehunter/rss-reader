class CreateFeedsUsersJoin < ActiveRecord::Migration
  def up
  	create_table 'feeds_users', :id => false do |t|
  		t.column 'feed_id', :integer
  		t.column 'user_id', :integer
  	end
  end

  def down
  	drop_table 'feeds_users'
  end
end

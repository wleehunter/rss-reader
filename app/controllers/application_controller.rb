class ApplicationController < ActionController::Base
  protect_from_forgery

  def user_logged_in?
  	return !session[:email].nil?
  end
end

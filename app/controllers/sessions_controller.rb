class SessionsController < ApplicationController

	def new
		if !session[:email].nil?
			@user = User.find_by_email(session[:email].downcase)
			respond_to do |format|
		    format.html {redirect_to @user, notice: 'User logged in.'}
	      format.json { render json: @user, status: :created, location: @user }
	    end
		end

	end

	def create
		@user = User.find_by_email(params[:session][:email].downcase)
	  if @user && @user.authenticate(params[:session][:password])
	    session[:name] = @user.name
	    session[:email] = @user.email

		  respond_to do |format|
		    format.html {redirect_to @user, notice: 'User was successfully logged in.'}
	      format.json { render json: @user, status: :created, location: @user }
	    end
		else  	
		    # Create an error message and re-render the signin form.
	  end
	end

	def destroy

	end
end
